# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

from typing import Sequence, Tuple, Optional

import discord
from discord.ext import menus


class EmbedFieldSource(menus.ListPageSource):
    """Menu page source that paginates embed fields.
    `entries` should be a list of tuples (name, value) representing embed fields.
    `inline` specifies whether the fields will be inline.
    `template` is an optional template embed.
    Every part of it will be used as-is except for the footer and fields.
    If template is not set, `title` can be used to give the embed a title.
    The default template is blurple."""

    def __init__(
        self,
        entries: Sequence[Tuple[str, str]],
        *,
        per_page: int = 5,
        inline: bool = False,
        template: Optional[discord.Embed] = None,
        title: Optional[str] = None,
    ):
        if not template:
            template = discord.Embed(title=title, colour=discord.Colour.blurple())

        self.template = template
        self.inline = inline

        super().__init__(entries, per_page=per_page)

    async def format_page(self, menu: menus.Menu, page: Sequence[Tuple[str, str]]):
        embed = self.template.copy()
        embed.clear_fields()
        if self.get_max_pages() > 1:
            embed.set_footer(text=f"Page {menu.current_page+1}/{self.get_max_pages()}")

        for field in page:
            embed.add_field(name=field[0], value=field[1], inline=self.inline)

        return embed


class EmbedStringSource(menus.ListPageSource):
    """Menu page source that paginates strings in embeds.
    `entries` should be a list of strings.
    `template` is an optional template embed.
    Every part of it will be used as-is except for the footer and description.
    If template is not set, `title` can be used to give the embed a title.
    The default template is blurple."""

    def __init__(
        self,
        entries: Sequence[str],
        *,
        per_page: int = 20,
        template: Optional[discord.Embed] = None,
        title: Optional[str] = None,
    ):
        if not template:
            template = discord.Embed(title=title, colour=discord.Colour.blurple())

        self.template = template

        super().__init__(entries, per_page=per_page)

    async def format_page(self, menu: menus.Menu, page: Sequence[str]):
        embed = self.template.copy()
        if self.get_max_pages() > 1:
            embed.set_footer(text=f"Page {menu.current_page+1}/{self.get_max_pages()}")
        embed.description = "\n".join(page)

        return embed
