# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

import enum


class PermissionLevel(enum.IntEnum):
    INVALID = 0
    USER = 1
    MODERATOR = 2
    MANAGER = 3
    ADMIN = 4

    def __str__(self):
        match self:
            case self.INVALID:
                return "[0] INVALID"
            case self.USER:
                return "[1] USER"
            case self.MODERATOR:
                return "[2] MODERATOR"
            case self.MANAGER:
                return "[3] MANAGER"
            case self.ADMIN:
                return "[4] ADMIN"
            case _:
                return f"[{int(self)}] UNKNOWN"


class ActionType:
    WARN = "warn"
    MUTE = "mute"
    UNMUTE = "unmute"
    KICK = "kick"
    BAN = "ban"
    UNBAN = "unban"

    def __str__(self) -> str:
        return ActionType[self]
