# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

import os


def _try_int(x: str | None):
    try:
        return int(x)
    except:
        return None


TOKEN = os.environ["TOKEN"]
DATABASE_URL = os.environ["DATABASE_URL"]
SENTRY_DSN = os.environ.get("SENTRY_DSN", None)

SYNC_COMMANDS = os.environ.get("SYNC_COMMANDS", "true") == "true"
COMMANDS_GUILD_ID = _try_int(os.environ.get("COMMANDS_GUILD_ID", None))
APPLICATION_ID = _try_int(os.environ.get("APPLICATION_ID", None))
