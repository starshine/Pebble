# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

import base64
import logging
import uuid
from pathlib import Path
from typing import Union

import asyncpg
import discord
import sentry_sdk
from discord import app_commands
from discord.ext import commands

from . import config
from .context import Context

extensions = (
    "config",
    "meta",
    "moderation",
)


class Pebble(commands.AutoShardedBot):
    _db: asyncpg.Pool

    def __init__(self):
        sentry_sdk.init(
            dsn=config.SENTRY_DSN,
            ignore_errors=[KeyboardInterrupt],
        )

        intents = discord.Intents(
            guilds=True,
            members=True,
            bans=True,
            emojis_and_stickers=True,
            webhooks=True,
            invites=True,
            messages=True,
            reactions=True,
            message_content=True,
        )

        app_id = config.APPLICATION_ID
        if not app_id:
            app_id = int(base64.b64decode(config.TOKEN.split(".")[0]))

        super().__init__(
            application_id=app_id,
            activity=discord.Activity(
                type=discord.ActivityType.watching, name=f"for commands"
            ),
            command_prefix=commands.when_mentioned,
            intents=intents,
            case_insensitive=True,
            strip_after_prefix=True,
            tree_cls=PebbleTree,
            allowed_mentions=discord.AllowedMentions(
                users=True, roles=False, everyone=False
            ),
        )

    async def setup_hook(self) -> None:
        self._db = await asyncpg.create_pool(config.DATABASE_URL)

        applied = await self._run_migrations()
        if applied:
            logging.info(f"Applied {applied} migration{'s' if applied != 1 else ''}!")

        for ext in extensions:
            logging.info(f'Loading cog "{ext}"')
            try:
                await self.load_extension(f"cogs.{ext}")
                logging.info(f'Loaded cog "{ext}"')
            except Exception as ex:
                logging.exception(f'Could not load cog "{ext}"', ex)

        if config.COMMANDS_GUILD_ID:
            logging.info(f"Only syncing commands to {config.COMMANDS_GUILD_ID}")
            self.tree.copy_global_to(guild=discord.Object(config.COMMANDS_GUILD_ID))

        if config.SYNC_COMMANDS:
            logging.info("Syncing commands")
            await self.tree.sync(
                guild=discord.Object(config.COMMANDS_GUILD_ID)
                if config.COMMANDS_GUILD_ID
                else None
            )

    async def on_error(self, event_method: str, *args, **kwargs) -> None:
        logging.exception(f'Exception in "{event_method}"')

    async def on_command_error(self, ctx: commands.Context, exc, /):
        # This prevents any commands with local handlers being handled here in on_command_error.
        if hasattr(ctx.command, "on_error"):
            return

        # This prevents any cogs with an overwritten cog_command_error being handled here.
        cog = ctx.cog
        if cog:
            if cog._get_overridden_method(cog.cog_command_error) is not None:
                return

        if isinstance(exc, commands.CommandNotFound):
            return

        if isinstance(
            exc,
            (
                commands.UserInputError,
                commands.CheckFailure,
                commands.CommandOnCooldown,
                commands.DisabledCommand,
            ),
        ):
            await ctx.send(f":x: {exc}")
            return

        # internal error, so report to Sentry
        event_id = sentry_sdk.capture_exception(exc) or str(uuid.uuid4())
        logging.exception(
            f'[Error code: {event_id}] Exception in command "{ctx.command}": {exc}',
            exc_info=exc,
        )

        try:
            embed = discord.Embed(
                title="Internal error occurred",
                description="If you want to report this error to the developer, "
                + "please include the error code above (in plain text) "
                + "with a description of what you were doing at the time "
                + "to cause the error.",
                colour=discord.Colour.red(),
                timestamp=discord.utils.utcnow(),
            )
            embed.set_footer(text=event_id)

            await ctx.send(
                content=f"> **Error code:** `{event_id}`",
                embeds=[embed],
            )
        except discord.Forbidden:
            pass

    async def get_context(
        self,
        origin: Union[discord.Interaction, discord.Message],
        /,
        *,
        cls=Context,
    ) -> Context:
        return await super().get_context(origin, cls=cls)

    async def run(self) -> None:
        await self.start(token=config.TOKEN)

    @property
    def db(self):
        return self._db

    async def on_ready(self):
        logging.info(f"Ready as {self.user}!")

        async with self.db.acquire() as con:
            for guild in self.guilds:
                await con.execute(
                    "insert into guilds (id) values ($1) on conflict (id) do nothing",
                    guild.id,
                )

    async def on_guild_join(self, guild: discord.Guild):
        logging.info(f'Joined guild "{guild.name}" ({guild.id})')

        await self.db.execute(
            "insert into guilds (id) values ($1) on conflict (id) do nothing",
            guild.id,
        )

    async def _run_migrations(self) -> int:
        """Runs pending migrations, if any. Returns the number of applied migrations."""

        migrations = []
        for file in Path("pebble/core/migrations").iterdir():
            if file.is_file():
                migrations.append((file.name, file.read_text()))

        migrations.sort(key=lambda m: m[0])
        migrations = [m[1] for m in migrations]

        try:
            current: int = await self._db.fetchval("select schema_version from info")
        except:
            current: int = 0

        applied = 0
        async with self._db.acquire() as con:
            async with con.transaction():
                for m in migrations[current:]:
                    # noinspection PyBroadException
                    try:
                        logging.info(f"Executing migration {current + 1}")
                        await self._db.execute(m)
                        logging.info(f"Executed migration {current + 1}")
                        current += 1
                        applied += 1
                    except Exception as e:
                        logging.exception(
                            f"Error executing migration {current + 1}", exc_info=e
                        )

        return applied


class PebbleTree(app_commands.CommandTree):
    def __init__(self, client: Pebble):
        super().__init__(client)

    async def on_error(
        self, interaction: discord.Interaction, error: app_commands.AppCommandError
    ) -> None:
        if isinstance(error, app_commands.CommandNotFound):
            return

        if isinstance(
            error,
            (
                app_commands.CheckFailure,
                app_commands.CommandOnCooldown,
                app_commands.TransformerError,
            ),
        ):
            return await interaction.response.send_message(
                content=error, ephemeral=True
            )

        # internal error, log to Sentry and show a message
        event_id = sentry_sdk.capture_exception(error) or str(uuid.uuid4())
        logging.exception(f"[Error code: {event_id}] Exception in interaction", error)

        embed = discord.Embed(
            title="Internal error occurred",
            description="If you want to report this error to the developer, "
            + "please include the error code above (in plain text) "
            + "with a description of what you were doing at the time "
            + "to cause the error.",
            colour=discord.Colour.red(),
            timestamp=discord.utils.utcnow(),
        )
        embed.set_footer(text=event_id)

        try:
            if interaction.response.is_done():
                await interaction.channel.send(
                    content=f"> **Error code:** `{event_id}`", embeds=[embed]
                )
            else:
                await interaction.response.send_message(
                    content=f"> **Error code:** `{event_id}`",
                    embeds=[embed],
                    ephemeral=True,
                )
        except discord.Forbidden:
            pass
