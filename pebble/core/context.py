# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

from typing import Optional, Sequence

import asyncpg
import discord
from discord.ext import commands


class _Confirm(discord.ui.View):
    value: Optional[bool]

    def __init__(
        self,
        *,
        user: discord.User,
        timeout: float = 60.0,
        delete_after: bool = False,
    ):
        super().__init__(timeout=timeout)

        self.value = None
        self.user = user
        self.delete_after = delete_after

    async def interaction_check(self, interaction: discord.Interaction) -> bool:
        if interaction.user and interaction.user.id == self.user.id:
            return True
        await interaction.response.send_message(
            content="This button cannot be used by you, sorry!", ephemeral=True
        )
        return False

    @discord.ui.button(label="Confirm", style=discord.ButtonStyle.green)
    async def confirm(
        self,
        interaction: discord.Interaction,
        button: discord.ui.Button,
    ):
        # disable buttons
        if not self.delete_after:
            for item in self.children:
                if hasattr(item, "disabled"):
                    item.disabled = True
            await interaction.response.edit_message(view=self)
        else:
            await interaction.message.delete()

        self.value = True
        self.stop()

    @discord.ui.button(label="Cancel", style=discord.ButtonStyle.grey)
    async def cancel(
        self,
        interaction: discord.Interaction,
        button: discord.ui.Button,
    ):
        # disable buttons
        if not self.delete_after:
            for item in self.children:
                if hasattr(item, "disabled"):
                    item.disabled = True
            await interaction.response.edit_message(view=self)
        else:
            await interaction.message.delete()

        self.value = False
        self.stop()


class Context(commands.Context):
    _db: asyncpg.Pool

    def __init__(self, *, message: discord.Message, bot, **kwargs):
        super().__init__(message=message, bot=bot, **kwargs)

        self._db = bot.db

    @property
    def pool(self):
        return self._db

    async def confirm(
        self,
        *,
        content: str = "Do you want to continue?",
        embed: Optional[discord.Embed] = None,
        embeds: Optional[Sequence[discord.Embed]] = None,
        timeout: float = 60.0,
        delete_after: bool = False,
        ephemeral: bool = False,
        timeout_message: str = "\N{CROSS MARK} Timed out.",
        cancel_message: str = "\N{CROSS MARK} Cancelled.",
    ) -> Optional[bool]:
        """Sends a confirmation message.

        Returns None if the view timed out, True or False otherwise.

        If the view timed out or was cancelled,
        either `timeout_message` or `cancel_message` is sent if they are not None."""

        user = self.interaction.user if self.interaction else self.author

        view = _Confirm(
            user=user,
            timeout=timeout,
            delete_after=delete_after,
        )

        embeds = [embed] if embed else embeds

        msg = await self.send(
            content=content, embeds=embeds, view=view, ephemeral=ephemeral
        )
        await view.wait()

        if view.value is None and timeout_message:
            await self.send(content=timeout_message)
        elif not view.value and cancel_message:
            await self.send(content=cancel_message)

        return view.value
