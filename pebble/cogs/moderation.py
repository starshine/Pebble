# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

import logging
from typing import Optional
import asyncpg
import discord
from discord.ext import commands

from core import (
    Context,
    Pebble,
    require_permission,
    PermissionLevel,
    ActionType,
    EmbedFieldSource,
    Paginator,
)


def _ordinal(num: int):
    mod = num % 100
    if (
        mod == 10
        or mod == 11
        or mod == 12
        or mod == 13
        or mod == 14
        or mod == 15
        or mod == 16
        or mod == 17
        or mod == 18
        or mod == 19
    ):
        return f"{num}th"

    match num % 10:
        case 1:
            return f"{num}st"
        case 2:
            return f"{num}nd"
        case 3:
            return f"{num}rd"
        case _:
            return f"{num}th"


class Moderation(commands.Cog):
    """Tools to moderate your server"""

    _bot: Pebble
    _db: asyncpg.Pool

    def __init__(self, bot: Pebble):
        self._bot = bot
        self._db = bot.db

    @commands.hybrid_command()
    @commands.bot_has_guild_permissions(kick_members=True)
    @require_permission(PermissionLevel.MANAGER)
    async def kick(self, ctx: Context, user: discord.Member, *, reason: str | None):
        """Kick a user from the server."""

        reason = reason or "N/A"

        # actor must have higher role than target (or be the guild owner)
        if (
            ctx.author.top_role.position <= user.top_role.position
            and not ctx.guild.owner_id == ctx.author.id
        ):
            return await ctx.send(
                "You are not high enough in the role hierarchy to do that.",
                ephemeral=True,
            )

        # bot must have permission to kick the user
        if ctx.guild.me.top_role.position <= user.top_role.position:
            return await ctx.send(
                "I am not high enough in the role hierarchy to do that.",
                ephemeral=True,
            )

        try:
            await user.send(
                f"You have been kicked from {ctx.guild.name}.\nReason: {reason}"
            )
        except discord.Forbidden:
            pass

        try:
            await ctx.guild.kick(user, reason=reason)
        except discord.HTTPException as e:
            return await ctx.send(f"I was unable to kick {user}.\nReason: {e}")

        async with ctx.pool.acquire() as conn:
            kick_id = await self._insert_log(
                ctx, conn, user.id, ActionType.KICK, reason
            )

            await self._send_mod_log(
                ctx,
                conn=conn,
                moderator=ctx.author,
                target=user,
                action_type=ActionType.KICK,
                action_id=kick_id,
                reason=reason,
            )

        await ctx.send(f"Kicked **{user}**")

    @commands.hybrid_command()
    @commands.bot_has_guild_permissions(ban_members=True)
    @require_permission(PermissionLevel.MANAGER)
    async def ban(
        self, ctx: Context, user: discord.User, delete_days: int = 0, *, reason: str
    ):
        """Ban a user from the server."""

        # actor must have higher role than target (or be the guild owner)
        if isinstance(user, discord.Member) and (
            ctx.author.top_role.position <= user.top_role.position
            and not ctx.guild.owner_id == ctx.author.id
        ):
            return await ctx.send(
                "You are not high enough in the role hierarchy to do that.",
                ephemeral=True,
            )

        # bot must have higher role than target
        if (
            isinstance(user, discord.Member)
            and ctx.guild.me.top_role.position <= user.top_role.position
        ):
            return await ctx.send(
                "I am not high enough in the role hierarchy to do that.",
                ephemeral=True,
            )

        try:
            await user.send(
                f"You have been banned from {ctx.guild.name}.\nReason: {reason}"
            )
        except discord.Forbidden:
            pass

        try:
            await ctx.guild.ban(user, reason=reason, delete_message_days=delete_days)
        except discord.HTTPException as e:
            return await ctx.send(f"I was unable to ban {user}.\nReason: {e}")

        async with ctx.pool.acquire() as conn:
            ban_id = await self._insert_log(ctx, conn, user.id, ActionType.BAN, reason)

            await self._send_mod_log(
                ctx,
                conn=conn,
                moderator=ctx.author,
                target=user,
                action_type=ActionType.BAN,
                action_id=ban_id,
                reason=reason,
            )

        await ctx.send(f"Banned **{user}**")

    @commands.hybrid_command()
    @require_permission(PermissionLevel.MODERATOR)
    async def warn(self, ctx: Context, user: discord.Member, *, reason: str):
        """Warn a user for their crimes."""

        # actor must have higher role than target (or be the guild owner)
        if (
            ctx.author.top_role.position <= user.top_role.position
            and not ctx.guild.owner_id == ctx.author.id
        ):
            return await ctx.send(
                "You are not high enough in the role hierarchy to do that.",
                ephemeral=True,
            )

        async with ctx.pool.acquire() as conn:
            warn_id = await self._insert_log(
                ctx, conn, user.id, ActionType.WARN, reason
            )

            await self._send_mod_log(
                ctx,
                conn=conn,
                moderator=ctx.author,
                target=user,
                action_type=ActionType.WARN,
                action_id=warn_id,
                reason=reason,
            )

        try:
            await user.send(
                content=f"You were warned in {ctx.guild.name}.\nReason: {reason}"
            )
        except discord.Forbidden:
            await ctx.send(
                content=f"\N{WARNING SIGN} I was unable to DM {user} about their warning."
            )

        count = await ctx.pool.fetchval(
            "select count(*) from mod_log where guild_id = $1 and user_id = $2 and action_type = $3",
            ctx.guild.id,
            user.id,
            str(ActionType.WARN),
        )

        await ctx.send(f"Warned **{user}**, this is their {_ordinal(count)} warning.")

    @commands.hybrid_command()
    @require_permission(PermissionLevel.MODERATOR)
    async def modlogs(self, ctx: Context, user: Optional[discord.User]):
        """Show a user's moderation history."""

        user = user or ctx.author

        rows = await ctx.pool.fetch(
            "select * from mod_log where guild_id = $1 and user_id = $2 order by time desc",
            ctx.guild.id,
            user.id,
        )

        if len(rows) == 0:
            return await ctx.send(
                content=f"{user} does not have any moderation history.",
                ephemeral=True,
            )

        fields = [
            (
                f"#{r['id']} | {r['action_type']} | <t:{int(r['time'].timestamp())}>",
                f"**Moderator:** <@!{r['mod_id']}>\n**Reason:** {r['reason']}",
            )
            for r in rows
        ]

        tmpl = discord.Embed(
            title="Moderation history", colour=discord.Colour.blurple()
        )
        tmpl.set_author(name=user, icon_url=user.display_avatar.url)
        menu = Paginator(
            ctx=ctx,
            source=EmbedFieldSource(fields, template=tmpl, inline=False),
        )
        await menu.start()

    async def _insert_log(
        self,
        ctx: Context,
        conn: asyncpg.Connection,
        user_id: int,
        action_type: ActionType,
        reason: str,
    ) -> int:
        """Creates a mod log entry and returns its ID."""

        return await conn.fetchval(
            """insert into mod_log
            (guild_id, user_id, mod_id, action_type, reason)
            values ($1, $2, $3, $4, $5)
            returning id""",
            ctx.guild.id,
            user_id,
            ctx.author.id,
            str(action_type),
            reason,
        )

    async def _send_mod_log(
        self,
        ctx: Context,
        *,
        conn: asyncpg.Connection,
        moderator: discord.User,
        target: discord.User,
        action_type: ActionType,
        action_id: int,
        reason: str,
    ):
        """Sends a message to the guild's mod log. Returns the new mod log message on success."""

        channel_id = await conn.fetchval(
            "select mod_log from guilds where id = $1", ctx.guild.id
        )
        if not channel_id:
            logging.debug(f"Guild {ctx.guild.id} does not have a mod log channel set.")
            return

        channel = ctx.guild.get_channel(channel_id)
        if not channel:
            raise f"Unknown mod log channel {channel_id}."

        embed = discord.Embed(
            title=f"{action_type} | case {action_id}",
            timestamp=discord.utils.utcnow(),
        )
        embed.set_footer(text=f"ID: {target.id}")

        embed.description = f"**Offender:** {target} {target.mention}\n**Moderator:** {moderator} {moderator.mention}\n**Reason:** {reason}"
        match action_type:
            case ActionType.WARN:
                embed.colour = discord.Colour.yellow()
            case ActionType.MUTE, ActionType.KICK, ActionType.BAN:
                embed.colour = discord.Colour.orange()
            case ActionType.UNMUTE, ActionType.UNBAN:
                embed.colour = discord.Colour.green()
            case _:
                embed.colour = discord.Colour.blurple()

        msg = await channel.send(embeds=[embed])

        await conn.execute(
            "update mod_log set channel_id = $1, message_id = $2 where id = $3",
            channel.id,
            msg.id,
            action_id,
        )

        return msg


async def setup(bot: Pebble):
    await bot.add_cog(Moderation(bot))
