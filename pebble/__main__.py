# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import os
import logging

from dotenv import load_dotenv

load_dotenv()

from core.bot import Pebble


async def main():
    logging.basicConfig(
        style="{",
        format="[{asctime} {levelname}] [{name}] {message}",
        level=logging.INFO,
        datefmt="%H:%M:%S",
    )

    if os.name != "nt":
        import uvloop

        uvloop.install()

    try:
        await Pebble().run()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    asyncio.run(main())
